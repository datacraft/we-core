package com.uxuexi.core.web.base.page;

import java.util.List;

import org.nutz.dao.pager.Pager;
import org.nutz.log.Log;
import org.nutz.log.Logs;

public class VueTablePagination extends Pager {

	/**
	 * 当前页的数据
	 */
	private List<?> data;

	private static final long serialVersionUID = 8848523495013555357L;

	private static final Log log = Logs.get();

	private int pageNumber;
	private int pageSize;
	private int pageCount;
	private int recordCount;

	private int total;
	private int current_page;
	private int per_page;
	private int last_page;

	private int from;
	private int to;

	public VueTablePagination() {
		pageNumber = 1;
		current_page = pageNumber;

		pageSize = DEFAULT_PAGE_SIZE;
		per_page = pageSize;
	}

	@Override
	public VueTablePagination resetPageCount() {
		pageCount = -1;
		last_page = -1;
		return this;
	}

	@Override
	public VueTablePagination setPageNumber(int pn) {
		if (1 > pn && log.isInfoEnabled())
			log.infof("PageNumber shall start at 1, but input is %d, that mean pager is disable", pn);
		pageNumber = pn;
		current_page = pageNumber;
		return this;
	}

	@Override
	public VueTablePagination setPageSize(int pageSize) {
		this.pageSize = (pageSize > 0 ? pageSize : DEFAULT_PAGE_SIZE);
		this.per_page = this.pageSize;
		return resetPageCount();
	}

	@Override
	public VueTablePagination setRecordCount(int recordCount) {
		this.recordCount = recordCount > 0 ? recordCount : 0;
		this.pageCount = (int) Math.ceil((double) recordCount / pageSize);

		this.total = this.recordCount;
		this.last_page = this.pageCount;
		return this;
	}

	@Override
	public int getOffset() {
		return pageSize * (pageNumber - 1);
	}

	/**
	 * 获得item列表
	 */
	public List<?> getData() {
		return data;
	}

	/**
	 * 设置item列表
	 * 
	 * @param data
	 */
	public void setData(List<?> data) {
		this.data = data;
	}

	/*vue-table getters*/

	public int getTotal() {
		return total;
	}

	public int getCurrent_page() {
		return current_page;
	}

	public int getPer_page() {
		return per_page;
	}

	public int getLast_page() {
		return last_page;
	}

	public int getFrom() {
		from = getOffset();
		return from;
	}

	public int getTo() {
		to = this.pageNumber * this.pageSize;
		if (this.pageNumber * this.pageSize > this.recordCount) {
			to = this.recordCount;
		}
		return to;
	}

	/////////////////////////////////

	@Override
	public int getPageCount() {
		if (pageCount < 0)
			pageCount = (int) Math.ceil((double) recordCount / pageSize);
		return pageCount;
	}

	@Override
	public int getPageNumber() {
		return pageNumber;
	}

	@Override
	public int getPageSize() {
		return pageSize;
	}

	@Override
	public int getRecordCount() {
		return recordCount;
	}

	@Override
	public String toString() {
		return String
				.format("size: %d, total: %d, page: %d/%d", pageSize, recordCount, pageNumber, this.getPageCount());
	}

	@Override
	public boolean isFirst() {
		return pageNumber == 1;
	}

	@Override
	public boolean isLast() {
		if (pageCount == 0)
			return true;
		return pageNumber == pageCount;
	}

	@Override
	public boolean hasNext() {
		return !isLast();
	}

	@Override
	public boolean hasPrevious() {
		return !isFirst();
	}
}