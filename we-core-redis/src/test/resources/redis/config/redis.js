var ioc = {
	redisDs : {
		type : 'com.uxuexi.core.redis.support.impl.RedisDataSource',
		fields : {
			config : {
				type : 'com.uxuexi.core.redis.support.RedisConfig',
				args : [ '127.0.0.1', 6379, 30000,'123']
			}
		}
	},
	redis : {
		type : 'com.uxuexi.core.redis.RedisDao',
		args : [ {
			refer : "redisDs"
		} ]
	}
}