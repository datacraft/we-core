/**
 * TestJedis.java
 * com.uxuexi.core.redis
 * Copyright (c) 2018, 北京科技有限公司版权所有.
*/

package com.uxuexi.core.redis;

import org.nutz.log.Log;
import org.nutz.log.Logs;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCommands;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * 测试redis连接
 * <p>
 *
 * @author   朱晓川
 * @Date	 2018年2月26日 	 
 */
public class TestJedis {

	public static final Log logger = Logs.get();
	// Jedispool
	JedisCommands jedisCommands;
	JedisPool jedisPool;
	JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
	String ip = "127.0.0.1";
	int port = 6379;
	int timeout = 2000;
	/**
	 * 测试redis使用密码连接
	 * <p>
	 * 注意:windows 通过redis-server.exe启动的时候必须是命令行方式指定配置文件时，requirepass参数才有用
	 */
	String password = "sftz-redis-360";

	public TestJedis() {
		// 初始化jedis
		// 设置配置
		jedisPoolConfig.setMaxTotal(1024);
		jedisPoolConfig.setMaxIdle(100);
		jedisPoolConfig.setMaxWaitMillis(100);
		jedisPoolConfig.setTestOnBorrow(false);//jedis 第一次启动时，会报错
		jedisPoolConfig.setTestOnReturn(true);
		// 初始化JedisPool
		jedisPool = new JedisPool(jedisPoolConfig, ip, port, timeout, password);
		//  
		Jedis jedis = jedisPool.getResource();

		jedisCommands = jedis;
	}

	public void setValue(String key, String value) {
		this.jedisCommands.set(key, value);
	}

	public String getValue(String key) {
		return this.jedisCommands.get(key);
	}

	public static void main(String[] args) {
		TestJedis testJedis = new TestJedis();
		testJedis.setValue("testJedisKey", "AAA");
		logger.info("get value from redis:" + testJedis.getValue("testJedisKey"));
	}

}
